package com.zuhaib.autolocationfinder

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LocationResponse(
    @SerializedName("cityName")
    @Expose var cityName: String? = null,
    @SerializedName("latitude")
    @Expose var latitude: String? = null,
    @SerializedName("longitude")
    @Expose var longitude: String? = null,
    @SerializedName("city")
    @Expose var city: String? = null,
    @SerializedName("countryCode")
    @Expose var countryCode: String? = null,
    @SerializedName("countryName")
    @Expose var countryName: String? = null
)