package com.zuhaib.autolocationfinder

interface OnAutoLocationCallback {

    fun locationError(error: String?)
    fun locationCoordinate(lat: Double, lng: Double)
    fun locationResult(location: LocationResponse)
}