package com.zuhaib.autolocationfinder

enum class ErrorType(val value : String){
    NO_GPS("No GPS"),
    NO_INTERNET("No Internet"),
    NO_PERMISSION("Allow Permission"),
    INIT_GPS("Initialize your gps location"),
    EXCEPTION("EXCEPTION"),
    RESPONSE_FAIL("RESPONSE FAIL"),
    SLOW_INTERNET("Slow Internet")
}