package com.zuhaib.autolocationfinder

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.core.content.ContextCompat


fun checkPermission(mContext: Context): Boolean {
    val FirstPermissionResult = ContextCompat.checkSelfPermission(
        mContext,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )
    val SecondPermissionResult = ContextCompat.checkSelfPermission(
        mContext,
        Manifest.permission.ACCESS_FINE_LOCATION
    )
    return FirstPermissionResult == PackageManager.PERMISSION_GRANTED &&
            SecondPermissionResult == PackageManager.PERMISSION_GRANTED
}


fun Context.isOnline() : Boolean {
    val netInfo = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        val capabilities = netInfo.getNetworkCapabilities(netInfo.activeNetwork)
        if(capabilities != null) {
            when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                    return true
                }
            }
        }
    } else {
        val activeNetworkInfo = netInfo.activeNetworkInfo
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
            return true
        }
    }
    return false
}

fun Context.isGpsEnable() : Boolean{
    val manager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    return manager.isProviderEnabled(LocationManager.GPS_PROVIDER) || manager.isProviderEnabled(
        LocationManager.NETWORK_PROVIDER
    )
}

fun getTwoDecimalPlaces(number: Double): Double {
    return (number * 100).toInt() / 100.0
}

fun getThreeDecimalPlaces(number: Double): Double {
    return (number * 1000).toInt() / 1000.0
}

fun getFiveDecimalPlaces(number: Double): Double {
    return (number * 100000).toInt() / 100000.0
}

fun displacementCalculation(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
    val theta = lon1 - lon2
    var dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) +
            Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
            Math.cos(deg2rad(theta))
    dist = Math.acos(dist)
    dist = rad2deg(dist)
    dist = dist * 60 * 1.1515
    dist = dist * 1.609344
    return dist
}

private fun deg2rad(deg: Double): Double {
    return deg * Math.PI / 180.0
}

private fun rad2deg(rad: Double): Double {
    return rad * 180.0 / Math.PI
}