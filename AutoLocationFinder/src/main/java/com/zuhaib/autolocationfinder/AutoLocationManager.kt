package com.zuhaib.autolocationfinder

import android.content.Context
import android.location.Location
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener

class AutoLocationManager(
    private val mContext: Context,
    private val callback: OnAutoLocationCallback
 ) : OnMapReadyCallback {

    private var fusedLocationProviderClient : FusedLocationProviderClient
    private val FASTEST_INTERVAL = 2 * 1000.toLong()
    private val MINIMUM_DISPLACEMENT = 20 * 1000.0f

    init {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(mContext)
    }

    private fun initAutoLocation() : LocationSettingsRequest {
        val mLocationRequest = LocationRequest()
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL)
        mLocationRequest.setSmallestDisplacement(MINIMUM_DISPLACEMENT)
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest)
        return builder.build()
    }

    fun findGPSCoordinates(){
        val settingsClient = LocationServices.getSettingsClient(mContext)
        settingsClient.checkLocationSettings(initAutoLocation())
        if(checkPermission(mContext) && mContext.isGpsEnable() && mContext.isOnline()){
            fusedLocationProviderClient.lastLocation.addOnSuccessListener(
                OnSuccessListener<Location?> { location ->
                    if (location == null) callback.locationError(ErrorType.INIT_GPS.value)
                    else callback.locationCoordinate(location.latitude, location.longitude)
                }).addOnFailureListener(OnFailureListener { Exception ->
                callback.locationError(ErrorType.EXCEPTION.value)
            })
        }else{
            if(!checkPermission(mContext)){
                callback.locationError(ErrorType.NO_PERMISSION.value)
            }else if(!mContext.isGpsEnable()){
                callback.locationError(ErrorType.NO_GPS.value)
            }else{
                callback.locationError(ErrorType.NO_INTERNET.value)
            }
        }
    }

    fun isDisplacementPerform(lat1: Double,lng1: Double,lat2: Double,lng2: Double) : Double {
        var displacement : Double  = 0.0
        val currentPlaceLat: Double = getTwoDecimalPlaces(lat1)
        val currentPlaceLng: Double = getTwoDecimalPlaces(lng1)
        val lastPlaceLat = getTwoDecimalPlaces(lat2)
        val lastPlaceLng = getTwoDecimalPlaces(lng2)
        displacement  = displacementCalculation(lastPlaceLat,lastPlaceLng,currentPlaceLat,currentPlaceLng)
        return displacement
    }

    fun checkCoordinatesRadius(lat1: Double,lng1: Double,lat2: Double,lng2: Double, radius: Int){
        val isDisplacement =  isDisplacementPerform(lat1, lng1, lat2, lng2)
        if(isDisplacement > radius){ }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        if (checkPermission(mContext)) googleMap!!.setMyLocationEnabled(true)
    }

}